player createDiaryRecord ["Diary", 
							["Overview", 
							"On the forums you will find a detailed record of each PDT course in this mission.
							<br/>Within these records you can find detailed information about the course it self, qualified instructors and a list of people who have already passed the course."]
						 ];

player createDiaryRecord ["Diary", 
							["PDT", 
							"This is the Reality Gaming Task Force - PDT Training Camp. 
							<br/>Core training forms the bulk of courses within RG. It is aimed at advancing infantry members right from their basic combat training (BCT) through weapons and equipment qualification right up to specialisms of their chosen path. 
							<br/>
							<br/>It also acts as a gateway to other sections as demonstrating the key skills required will make you eligible and desirable for those teams."]
						 ];
