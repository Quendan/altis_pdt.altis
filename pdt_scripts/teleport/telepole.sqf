_pole = _this select 0;

_pole addAction ["Teleport to Infantry HQ", {call teleportplayer}, pole_inf_hq];
_pole addAction ["Teleport to Air Corps HQ", {call teleportplayer}, pole_air_hq];
_pole addAction ["Teleport to Motor Pool",{call teleportplayer}, pole_veh_hq];
_pole addAction ["Teleport to Drone Facility",{call teleportplayer}, pole_drone];
_pole addAction ["Teleport to Boat Harbour",{call teleportplayer}, pole_harbour];
_pole addAction ["Teleport to Pistol and AT Range",{call teleportplayer}, pole_pistol];
_pole addAction ["Teleport to Rifle Range",{call teleportplayer}, pole_rifle];
_pole addAction ["Teleport to Marksman Range",{call teleportplayer}, pole_marksman];
_pole addAction ["Teleport to Agia Marina",{call teleportplayer}, pole_agia];
_pole addAction ["Teleport to Kamino Firing Range",{call teleportplayer}, pole_kamino];
_pole addAction ["Teleport to Air Station Mike",{call teleportplayer}, pole_mike];
_pole addAction ["Teleport to Drop Off Point",{call teleportplayer}, pole_dz];
_pole allowDamage false; _pole enablesimulation false;

teleportplayer = {
_player = _this select 1;
_player allowDamage false;
_target = _this select 3;
_player setPos getPos _target;
sleep 1;
_player allowDamage true;
};