_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#FF0000'>Request Speedboat HMG</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Boat_Armed_01_hmg_F", _spawnpoint]]; 
_hemmo addAction["<t color='#FF0000'>Request Assault Boat</t>",      "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Boat_Transport_01_F", _spawnpoint]]; 
_hemmo addAction["<t color='#FF0000'>Request Rescue Boat</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Lifeboat", _spawnpoint]]; 
_hemmo addAction["<t color='#FF0000'>Request SDV</t>",               "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_SDV_01_F", _spawnpoint]]; 