_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#FF0000'>Request BTR-K Kamysh</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_APC_Tracked_02_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request ZSU-39 Tigris</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_APC_Tracked_02_AA_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request T-100 Varsuk</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_MBT_02_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request 2S9 Sochor</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_MBT_02_arty_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request MSE-3 Marid</t>",      "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_APC_Wheeled_02_rcws_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Ifrit</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_MRAP_02_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Ifrit HMG</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_MRAP_02_hmg_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Ifrit GMG</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_MRAP_02_gmg_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Quadbike</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Quadbike_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Offroad</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_G_Offroad_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Offroad (Armed)</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_G_Offroad_01_armed_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Tempest Transport (Covered)</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_03_covered_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Tempest Transport</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_03_transport_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Tempest Repair</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_03_repair_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Tempest Medical</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_03_ammo_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Tempest Ammo</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_03_fuel_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Tempest Fuel</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_03_medical_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Zamak Transport (Covered)</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_02_covered_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Zamak Transport</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_02_transport_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Zamak Repair</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_02_box_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Zamak Medical</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_02_medical_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Zamak Ammo</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_02_Ammo_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request Zamak Fuel</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_Truck_02_fuel_F", _spawnpoint]];