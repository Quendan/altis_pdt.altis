_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;                     

_hemmo addAction["<t color='#FF0000'>Request Tayran AR-2</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_UAV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request K40 Ababil-3 ATGM</t>","pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_UAV_02_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request K40 Ababil-3 GBU</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_UAV_02_CAS_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request UGV Saif</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_UGV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF0000'>Request UGV Saif RCWS</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["O_UGV_01_rcws_F", _spawnpoint]];