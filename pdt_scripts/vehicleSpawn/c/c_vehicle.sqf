_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#FF7F24'>Request Offroad</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Offroad_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Quadbike</t>",           "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Quadbike_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Hatchback</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Hatchback_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Hatchback (Sport)</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Hatchback_01_sport_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request SUV</t>",                "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_SUV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Truck</t>",              "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Van_01_transport_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Truck Boxer</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Van_01_box_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Fuel Truck</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Van_01_fuel_F", _spawnpoint]];