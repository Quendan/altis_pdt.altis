_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#FF7F24'>Request Rescue Boat</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Rubberboat", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Motorboat</t>",           "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Boat_Civil_01_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Motorboat (Rescue)</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Boat_Civil_01_rescue_F", _spawnpoint]];
_hemmo addAction["<t color='#FF7F24'>Request Motorboat (Police)</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C_Boat_Civil_01_police_F", _spawnpoint]];