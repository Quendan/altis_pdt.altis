_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;                    

_hemmo addAction["<t color='#0000FF'>Request Speedboat Minigun</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Boat_Armed_01_minigun_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Assault Boat</t>",      "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Boat_Transport_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Rescue Boat</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Lifeboat", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request SDV</t>",               "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_SDV_01_F", _spawnpoint]];