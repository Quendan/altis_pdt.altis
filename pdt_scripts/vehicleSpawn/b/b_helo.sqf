_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#0000FF'>Request UH-80 Ghost Hawk</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Heli_Transport_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request UH-80 Ghost Hawk Camo</t>",   "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Heli_Transport_01_camo_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request AH-99 Blackfoot</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Heli_Attack_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request MH-9 Hummingbird</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Heli_Light_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request AH-9 Pawnee</t>",             "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Heli_Light_01_armed_F", _spawnpoint]];