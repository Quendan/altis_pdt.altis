_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;                     

_hemmo addAction["<t color='#0000FF'>Request AR-2 Darter</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_UAV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request MQ4A Greyhawk ATGM</t>","pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_UAV_02_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request MQ4A Greyhawk GBU</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_UAV_02_CAS_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request UGV Stomper</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_UGV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request UGV Stomper RCWS</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_UGV_01_rcws_F", _spawnpoint]];