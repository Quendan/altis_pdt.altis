_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#0000FF'>Request IFV-6c Panther</t>",             "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_APC_Tracked_01_rcws_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request CRV-6e Bobcat</t>",              "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_APC_Tracked_01_CRV_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request IFV-6a Cheetah</t>",             "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_APC_Tracked_01_AA_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request M2A1 Slammer</t>",               "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MBT_01_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request M2A4 Slammer UP</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MBT_01_TUSK_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request M4 Scorcher</t>",                "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MBT_01_arty_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request M5 Sandstorm MLRS</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MBT_01_mlrs_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request AMV-7 Marshall</t>",             "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_APC_Wheeled_01_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Hunter</t>",                     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MRAP_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Hunter GMG</t>",                 "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MRAP_01_gmg_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Hunter HMG</t>",                 "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_MRAP_01_hmg_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Quadbike</t>",                   "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Quadbike_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Transport</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_transport_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Transport (Covered)</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_covered_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Offroad</t>",                    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_G_Offroad_01_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request Offroad (Armed)</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_G_Offroad_01_armed_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Mover</t>",                "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_mover_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Box</t>",                  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_box_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Repair</t>",               "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_Repair_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Ammo</t>",                 "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_ammo_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Fuel</t>",                 "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_fuel_F", _spawnpoint]];
_hemmo addAction["<t color='#0000FF'>Request HEMTT Medical</t>",              "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["B_Truck_01_medical_F", _spawnpoint]];