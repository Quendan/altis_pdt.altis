/*
* Created by Tier1
* Edited by [RG]eRazeri
*/
_vehicle = _this select 3 select 0;
_spawn = _this select 3 select 1;

if (local (_this select 1)) then {

  _nearestTargets = nearestObjects [getPosATL _spawn, ["Allvehicles"], 5];
   
  if (((count _nearestTargets) > 0) || (sdelay > 60)) then {

		hint format["Not enough room or try again in about %1 seconds.", sdelay - 60];
		playsound "warning1";
    sleep 5;
    hint "";

	} else {
  
		_veh = _vehicle createVehicle (getPos _spawn);
    
    _veh setpos [(getpos _veh) select 0, (getpos _veh) select 1, ((getpos _veh) select 2) + 0.5];
		_veh setDir (getDir _spawn);
        
    _varName = format ["%1_%2",_vehicle,laskuri];
    _veh SetVehicleVarName _varName;
	  _veh call Compile format ["%1=_this ; PublicVariable ""%1""",_varName];
    	
		laskuri = laskuri + 1;
		sdelay = sdelay + 120;
		playSound "confirm1";
	};
};
