_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#888888'>Request MBT-52 Kuma CTRG</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Kuma_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request AMV-7 Marshall CTRG</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Marshall_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request FV-720 Mora CTRG</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Mora_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request M5 Sandstorm MLRS CTRG</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Sandstorm_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request M4 Scorcher CTRG</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Scorcher_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request M2A1 Slammer CTRG</t>",      "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Slammer_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request M2A1 Slammer UP CTRG</t>",   "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Slammer_UP_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request IFV-6c Panther CTRG</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Panther_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request CRV-6e Bobcat CTRG</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Bobcat_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request IFV-6a Cheetah CTRG</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Cheetah_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request Hunter CTRG</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Hunter_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request Hunter HMG CTRG</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Hunter_HMG_CTRG", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request Hunter GMG CTRG</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Hunter_GMG_CTRG", _spawnpoint]];