_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;
_spawnpoint_1 = _this select 2;
_spawnpoint_2 = _this select 3;

_hemmo addAction["<t color='#888888'>Request F/A-18 E Super Hornet</t>",   "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["JS_JC_FA18E", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request F/A-18 F Super Hornet</t>",   "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["JS_JC_FA18F", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request MV-22 Osprey</t>",            "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["MV22_ported", _spawnpoint_1]];
_hemmo addAction["<t color='#888888'>Request C-130J Hercules</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["C130J_ported", _spawnpoint_2]];