_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#888888'>Request UH-80 Ghosthawk WD</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Ghosthawk_WD", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request RAH-66 Comanche WD</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Comanche_WD", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request AW159 Wildcat Armed WD</t>",      "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Wildcat_WD", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request AW159 Wildcat Transport WD</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Wildcat_unarmed_WD", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request HC3A Merlin WD</t>",              "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Merlin_WD", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request CH-47F Chinook</t>",                "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["CH_47F_EP1", _spawnpoint]];