_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#888888'>Request UH-80 Ghosthawk DS</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Ghosthawk_DS", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request RAH-66 Comanche DS</t>",          "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Comanche_DS", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request AW159 Wildcat Armed DS</t>",      "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Wildcat_DS", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request AW159 Wildcat Transport DS</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Wildcat_unarmed_DS", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request HC3A Merlin DS</t>",              "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["RG_Merlin_DS", _spawnpoint]];
_hemmo addAction["<t color='#888888'>Request CH-47F Chinook</t>",              "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["CH_47F_EP1", _spawnpoint]];