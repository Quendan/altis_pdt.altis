_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;

_hemmo addAction["<t color='#00FF00'>Request FV-720 Mora</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_APC_tracked_03_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request MBT-52 Kuma</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_MBT_03_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request AFV-4 Gorgon</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_APC_Wheeled_03_cannon_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Strider</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_MRAP_03_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Strider HMG</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_MRAP_03_hmg_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Strider GMG</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_MRAP_03_gmg_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Quadbike</t>",        "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Quadbike_01_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Zamak Transport (Covered)</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Truck_02_covered_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Zamak Transport</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Truck_02_transport_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Offroad</t>",         "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_G_Offroad_01_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Offroad (Armed)</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_G_Offroad_01_armed_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Zamak Ammo</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Truck_02_ammo_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Zamak Repair</t>",     "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Truck_02_box_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Zamak Medical</t>",    "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Truck_02_medical_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request Zamak Fuel</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_Truck_02_fuel_F", _spawnpoint]];