_hemmo = _this select 0;
hemmo_init = [_hemmo] execVM "pdt_scripts\vehicleSpawn\hemmo.sqf";

_spawnpoint = _this select 1;                      

_hemmo addAction["<t color='#00FF00'>Request AR-2 Darter</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_UAV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request K40 Ababil-3 ATGM</t>","pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_UAV_02_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request K40 Ababil-3 GBU</t>", "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_UAV_02_CAS_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request UGV Stomper</t>",       "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_UGV_01_F", _spawnpoint]];
_hemmo addAction["<t color='#00FF00'>Request UGV Stomper RCWS</t>",  "pdt_scripts\vehicleSpawn\spawnVehicle.sqf",["I_UGV_01_rcws_F", _spawnpoint]];