waitUntil {!isNil "bis_fnc_init"};
_crate = _this select 0;

if(local _crate) then {
clearMagazineCargoGlobal _crate;
clearMagazineCargo _crate;
clearWeaponCargoGlobal _crate;
clearWeaponCargo _crate;
clearItemCargoGlobal _crate;
clearItemCargo _crate;
clearBackpackCargo _crate;
clearBackpackCargoGlobal _crate;

_crate addWeaponCargoGlobal ["arifle_Mk20_GL_F", 99];

_crate addMagazineCargoGlobal ["1Rnd_HE_Grenade_shell", 999];
_crate addMagazineCargoGlobal ["1Rnd_SmokeBlue_Grenade_shell", 999];
_crate addMagazineCargoGlobal ["UGL_FlareCIR_F", 999];

};