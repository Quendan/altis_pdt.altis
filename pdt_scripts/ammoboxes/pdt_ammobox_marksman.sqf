waitUntil {!isNil "bis_fnc_init"};
_crate = _this select 0;

if(local _crate) then {
clearMagazineCargoGlobal _crate;
clearMagazineCargo _crate;
clearWeaponCargoGlobal _crate;
clearWeaponCargo _crate;
clearItemCargoGlobal _crate;
clearItemCargo _crate;
clearBackpackCargo _crate;
clearBackpackCargoGlobal _crate;


_crate addWeaponCargoGlobal ["arifle_MXM_Hamr_pointer_F", 99];
_crate addWeaponCargoGlobal ["srifle_EBR_MRCO_pointer_F", 99];

_crate addMagazineCargoGlobal ["20Rnd_762x51_Mag", 999];

_crate additemCargoGlobal ["optic_Hamr", 99];

};