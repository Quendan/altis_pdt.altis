waitUntil {!isNil "bis_fnc_init"};
_crate = _this select 0;

if(local _crate) then {
clearMagazineCargoGlobal _crate;
clearMagazineCargo _crate;
clearWeaponCargoGlobal _crate;
clearWeaponCargo _crate;
clearItemCargoGlobal _crate;
clearItemCargo _crate;
clearBackpackCargo _crate;
clearBackpackCargoGlobal _crate;

_crate addWeaponCargoGlobal ["LMG_Mk200_F", 99];

_crate addMagazineCargoGlobal ["200Rnd_65x39_cased_Box", 999];

_crate additemCargoGlobal ["optic_Hamr", 99];
};