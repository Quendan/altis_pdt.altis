waitUntil {!isNil "bis_fnc_init"};
_crate = _this select 0;

if(local _crate) then {
clearMagazineCargoGlobal _crate;
clearMagazineCargo _crate;
clearWeaponCargoGlobal _crate;
clearWeaponCargo _crate;
clearItemCargoGlobal _crate;
clearItemCargo _crate;
clearBackpackCargo _crate;
clearBackpackCargoGlobal _crate;

_crate addWeaponCargoGlobal ["hgun_Rook40_F", 99];

_crate addMagazineCargoGlobal ["16Rnd_9x21_Mag", 999];

};