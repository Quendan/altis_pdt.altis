waitUntil {!isNil "bis_fnc_init"};
_crate = _this select 0;

if(local _crate) then {
clearMagazineCargoGlobal _crate;
clearMagazineCargo _crate;
clearWeaponCargoGlobal _crate;
clearWeaponCargo _crate;
clearItemCargoGlobal _crate;
clearItemCargo _crate;
clearBackpackCargo _crate;
clearBackpackCargoGlobal _crate;

_crate addWeaponCargoGlobal ["launch_NLAW_F", 99];

_crate addMagazineCargoGlobal ["NLAW_F", 999];

_crate addBackpackCargoGlobal ["B_Carryall_khk", 99];

};
