gm_MenuOpen = false;

fnc_MenuKeyPressed = {
if (gm_MenuOpen) then {
call fnc_MenuClose;
} else {
call fnc_MenuOpen;
};
};

fnc_MenuOpen = {
gm_MenuOpen = true;
gm_RecruitAction = player addAction ['<t color="#00ff00">' + "++Recruit Target Unit++" + '</t>', {call fnc_RecruitTarget}];
gm_JoinAction = player addAction ['<t color="#00ff00">' + "++Join Target Group++" + '</t>', {call fnc_JoinTarget}];
gm_LeaveAction = player addAction ['<t color="#ff0000">' + "--Leave current group--" + '</t>', {call fnc_LeaveGroup}];
gm_CloseAction = player addaction ['<t color="#ffc600">' + "!!Close Group Management!!" + '</t>', {call fnc_MenuClose}];
};

fnc_MenuClose = {
player removeaction gm_CloseAction;
player removeaction gm_RecruitAction;
player removeaction gm_JoinAction;
player removeaction gm_LeaveAction;
gm_MenuOpen = false;
};

fnc_LeaveGroup = {
_side = side player;
_group = createGroup _side;
[player] joinSilent _group;
};

fnc_RecruitTarget = {
[cursorTarget] joinSilent player;
};

fnc_JoinTarget = {
[player] joinSilent cursorTarget;
};