private ["_unit","_corpse"];

_unit = _this select 0;
_corpse = _this select 1;

if (local _unit) then {
	_corpse removeAction Magoo_VASAction;
	_corpse removeAction Magoo_CAMAction;
	
			Magoo_VASAction = player addAction["<t color='#11FFFF'>Virtual Ammobox</t>", "scripts\VAS\open.sqf", nil, 1, false, True, "", "_this == Admin_1 || _this ==  Admin_2 || _this == Admin_3 || _this == Admin_4"];
			Magoo_CAMAction = player addAction ["Camera", "scripts\spectator\specta.sqf", nil, 1, false, True, "", "_this == Admin_1 || _this ==  Admin_2 || _this == Admin_3 || _this == Admin_4"];
};