if(!isDedicated) then {
private ["_playerRespawn"];
if (isNil "Admin_1") then {Admin_1 = objNull};
if (isNil "Admin_2") then {Admin_2 = objNull};
if (isNil "Admin_3") then {Admin_3 = objNull};
if (isNil "Admin_4") then {Admin_4 = objNull};
//Admin command access to select units
if ((local Admin_1) || (local Admin_2) || (local Admin_3) || (local Admin_4)) then {

			Magoo_VASAction = player addAction["<t color='#11FFFF'>Virtual Ammobox</t>", "scripts\VAS\open.sqf", nil, 1, false, True, "", "_this == Admin_1 || _this ==  Admin_2 || _this == Admin_3 || _this == Admin_4"];
			Magoo_CAMAction = player addAction ["Camera", "scripts\spectator\specta.sqf", nil, 1, false, True, "", "_this == Admin_1 || _this ==  Admin_2 || _this == Admin_3 || _this == Admin_4"];
			_playerRespawn = player addEventHandler ["Respawn", {_this execVM "scripts\AdminLoop\onPlayerRespawn.sqf";}]; 
			
};  
};