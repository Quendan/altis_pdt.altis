if(isDedicated) exitWith {};
private ["_playerRespawn"];
if (isNil "HMG_1") then {HMG_1 = objNull};
if (isNil "HMG_2") then {HMG_2 = objNull};
if (isNil "HMG_3") then {HMG_3 = objNull};
if (isNil "HMG_4") then {HMG_4 = objNull};
if ((local HMG_1) || (local HMG_2) || (local HMG_3) || (local HMG_4)) then {

      hmg_paukku_array = [["500Rnd_127x99_mag",500]];
			HMG_Spawn_Action = player addAction ["<t color='#ff1111'>Deploy HMG","scripts\mortar_and_hmg_spawn\HMG_spawn.sqf", nil, 1, false, True, "","_this == HMG_1 || _this ==   HMG_2 || _this ==  HMG_3 || _this ==  HMG_4"];
			_playerRespawn = player addEventHandler ["Respawn", {_this execVM "scripts\mortar_and_hmg_spawn\HMG_onPlayerRespawn.sqf";}]; 
};