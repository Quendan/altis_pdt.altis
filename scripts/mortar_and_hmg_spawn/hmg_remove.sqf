_HMG = _this select 0;
_player = _this select 1;
_id = _this select 2;

if(!isNull _player) then{

if (isNil "_HMG") exitWith {};

if (vehicle _player == _HMG) exitWith {hint "You have to get out of the HMG before you can remove it."; sleep 5; hint "";};

_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};
_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};
_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};
_player playMove "AinvPknlMstpSlayWrflDnon_medic";
sleep 5;
waitUntil {animationState _player != "AinvPknlMstpSlayWrflDnon_medic"};

if (!alive _player) exitWith {hint "You died before you could remove your HMG."; sleep 5; hint "";};
HMG_paukku_array = magazinesAmmo _HMG;
deleteVehicle _HMG;
HMG_Spawned = false;
hint "HMG removed."; 
sleep 5; hint "";
HMG_Spawn_Action = player addAction ["<t color='#ff1111'>Deploy HMG","scripts\mortar_and_hmg_spawn\HMG_spawn.sqf", nil, 1, false, True, "",""];//_this == Mortar_1 || _this ==   Mortar_2 || _this ==  Mortar_3 || _this ==  Mortar_4
};