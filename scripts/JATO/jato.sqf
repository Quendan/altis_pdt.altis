/*-------------------------------------------------------------------
very simple JATO script v0.9 by Sabre[Dust]

(Jet Assisted Take Off - actually Rocket Assisted Take Off)
put this in the init line of your aircraft:
nul = [this] execVM "scripts\JATO\jato.sqf"

or even better: use a trigger. This will allow you to easily reload the JATO System.

Make a trigger, set it to <Activation Bluefor, Present, Repeatedly> and put this in its <Condition> line:    
{typeof _x == "C130J"} count thislist >0   
Adjust the type of C130 you are using.
In its <On Act> field put this: 
_xhandle = [(thislist select 0)] execVM "scripts\JATO\jato.sqf"; hint "JATO ready";

Have Fun
-------------------------------------------------------------------*/


if ((typeOf (vehicle player) == "RG_PilotJet_DS") || (typeOf (vehicle player) == "RG_PilotJet_WD") || (typeOf (vehicle player) == "RG_PilotLead_WD") || (typeOf (vehicle player) == "RG_PilotLead_DS")) then {
while {true} do {
if ((vehicle player) isKindOf "C130J_ported") then {
jato = player addaction ['<t color="#ffc600">' + "Fire JATO" + '</t>',"scripts\JATO\jatoaction.sqf",[], 10, true, true, "SeagullFastForward", "driver vehicle player == player"];
jasl = player addaction ['<t color="#ffc600">' + "Fire JASL" + '</t>',"scripts\JATO\jaslaction.sqf",[], 9, true, true, "", "driver vehicle player == player"];
waitUntil {sleep 1; (!(typeOf (vehicle player) == "C130J_ported"))};
player removeaction jato;
player removeaction jasl;
};
sleep 10;
};
};