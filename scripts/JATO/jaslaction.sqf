/*-------------------------------------------------------------------
very simple JASL script v0.9 by Sabre_D
-------------------------------------------------------------------*/

_aircraft = vehicle player;

/*------------------------------------------------------
 check if crew or pilot is trying to access script
 use the following expression for human players only:
 if(player != driver _aircraft) then {
 for human players and AI use:
 if(isNull driver _aircraft) then {
 ------------------------------------------------------*/

if(player != driver _aircraft) then {
		_aircraft vehiclechat "Take your hands off the JASL System crewman! It's not a toy.";
} else {

	// remove the action and initialise a few variables
	_aircraft allowdamage false;
	player removeAction jasl;
	hint "JASL System ignition";
	_speed = -2;   // play with this to increase the power of the deceleration
	_rockettype = "M_Zephyr"; // object that gets attached, default "M_Maverick_AT"
	_boostcycle = 0;
		
	
	
	while {_boostcycle < 11 }  do {
		
		sleep 0.1; // interval at which the boost happens
		_boostcycle = _boostcycle+1; 
		_vel = velocity _aircraft;  // velocity of aircraft at current time 
		_dir = direction _aircraft; // vector of aircraft at current time
		// deceleration happens here
		_aircraft setVelocity [(_vel select 0)+(sin _dir*_speed),(_vel select 1)+ (cos _dir*_speed),(_vel select 2)+1];  
	
		if (_boostcycle == 1) then {
		
			// next step is attaching our _rockets 
	
			retrorocket1 = _rockettype createVehicle position _aircraft;
			retrorocket1 enablesimulation false;
			retrorocket2 = _rockettype createVehicle position _aircraft;
			retrorocket3 enablesimulation false;
			retrorocket3 = _rockettype createVehicle position _aircraft;
			retrorocket3 enablesimulation false;
			retrorocket4 = _rockettype createVehicle position _aircraft;
			retrorocket4 enablesimulation false;
			retrorocket5 = _rockettype createVehicle position _aircraft;
			retrorocket5 enablesimulation false;
			retrorocket6 = _rockettype createVehicle position _aircraft;
			retrorocket6 enablesimulation false;
			retrorocket7 = _rockettype createVehicle position _aircraft;
			retrorocket7 enablesimulation false;
			retrorocket8 = _rockettype createVehicle position _aircraft;
			retrorocket8 enablesimulation false;
			retrorocket1 attachTo [_aircraft,[1.5,9,-2.0]];
			retrorocket1 setDir 200;
			retrorocket2 attachTo [_aircraft,[-1.5,9,-2.0]];
			retrorocket2 setDir 160;
			retrorocket3 attachTo [_aircraft,[1.7,9,-2.4]];
			retrorocket3 setDir 200;
			retrorocket4 attachTo [_aircraft,[-1.7,9,-2.4]];
			retrorocket4 setDir 160;
			retrorocket5 attachTo [_aircraft,[1.9,9,-2.8]];
			retrorocket5 setDir 200;
			retrorocket6 attachTo [_aircraft,[-1.9,9,-2.8]];
			retrorocket6 setDir 160;
			retrorocket7 attachTo [_aircraft,[2.28,-0.1,-3.65]];
			retrorocket7 setVectorDirAndUp [[-0.4,0.3,0.7],[0.6,0.7,0.3]];	
			retrorocket8 attachTo [_aircraft,[-2.28,-0.1,-3.65]];
			retrorocket8 setVectorDirAndUp [[0.4,0.3,0.7],[0.6,0.7,0.3]];
	
			// ... and _smoke generators
	
			retrosmoke1 = "Smoke_82mm_AMOS_White" createVehicle position _aircraft;
			retrosmoke2 = "Smoke_82mm_AMOS_White" createVehicle position _aircraft;
			retrosmoke3 = "Smoke_82mm_AMOS_White" createVehicle position _aircraft;
			retrosmoke1 attachTo [_aircraft,[0,0,-3]];
			retrosmoke2 attachTo [_aircraft,[1.7,10.5,-2.4]];
			retrosmoke3 attachTo [_aircraft,[-1.7,10.5,-2.4]];
		};
	};
	
	sleep 2.0;
	
	// clean up the last set of _rockets before they explode
	
	hint "JASL System dropped";
	deleteVehicle retrorocket1;
	deleteVehicle retrorocket2;
	deleteVehicle retrorocket3;
	deleteVehicle retrorocket4;
	deleteVehicle retrorocket5;
	deleteVehicle retrorocket6;
	deleteVehicle retrorocket7;
	deleteVehicle retrorocket8;
	
	sleep 1.0;
	
	deleteVehicle retrosmoke1;
	deleteVehicle retrosmoke2;
	deleteVehicle retrosmoke3;
	_aircraft allowdamage true;
};