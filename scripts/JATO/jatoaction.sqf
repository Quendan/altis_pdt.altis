/*-------------------------------------------------------------------
very simple JATO script v0.9 by Sabre[Dust]
-------------------------------------------------------------------*/

_aircraft = (vehicle player);

/*------------------------------------------------------
 check if crew or pilot is trying to access script
 use the following expression for human players only:
 if(player != driver _aircraft) then {
 for human players and AI use:
 if(isNull driver _aircraft) then {
 ------------------------------------------------------*/

if(player != driver _aircraft) then {
		_aircraft vehiclechat "Take your hands off the JATO System crewman! It's not a toy.";
} else {

	// remove the action and initialise a few variables
	_aircraft allowdamage false;
	player removeAction jato;
	hint "JATO System ignition";
	_speed = 3;   // play with this to increase the power of the acceleration
	_rockettype = "M_Zephyr"; // object that gets attached, default "M_AT9_AT", try "R_MLRS" etc for fun
	_boostcycle = 0;
		
	while {_boostcycle < 30 }  do {
		
		sleep 0.3; // interval at which the boost happens
		_boostcycle = _boostcycle+1; 
		_vel = velocity _aircraft;  // velocity of aircraft at curetroent time 
		_dir = direction _aircraft; // vector of aircraft at curetroent time
		// acceleration happens here
		_aircraft setVelocity [(_vel select 0)+(sin _dir*_speed),(_vel select 1)+ (cos _dir*_speed),(_vel select 2)];  
		playSound3D ["A3\sounds_f_weapons\Rockets\rocket_fly_1.wss", _aircraft, false, position _aircraft, 100, 1, 200];
		
		if (_boostcycle == 1) then {
		
			// next step is attaching our rockets 
			
			rocket1a = _rockettype createVehicle position _aircraft;
			rocket1a enableSimulation false;
			rocket2a = _rockettype createVehicle position _aircraft;
			rocket2a enableSimulation false;
			rocket3a = _rockettype createVehicle position _aircraft;
			rocket3a enableSimulation false;
			rocket4a = _rockettype createVehicle position _aircraft;
			rocket4a enableSimulation false;
			rocket5a = _rockettype createVehicle position _aircraft;
			rocket5a enableSimulation false;
			rocket6a = _rockettype createVehicle position _aircraft;
			rocket6a enableSimulation false;
			rocket7a = _rockettype createVehicle position _aircraft;
			rocket7a enableSimulation false;
			rocket8a = _rockettype createVehicle position _aircraft;
			rocket8a enableSimulation false;
			rocket1a attachTo [_aircraft,[2.26,-1.1,-3.45]];
			rocket1a setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2a attachTo [_aircraft,[-2.26,-1.1,-3.45]];
			rocket2a setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
			rocket3a attachTo [_aircraft,[2.255,-1.1,-3.75]];
			rocket3a setVectorDirAndUp [[0.16,0.9,0.1],[0.84,0.1,0.9]];
			rocket4a attachTo [_aircraft,[-2.255,-1.1,-3.75]];
			rocket4a setVectorDirAndUp [[-0.16,0.9,0.1],[0.84,0.1,0.9]];
			rocket5a attachTo [_aircraft,[2.25,-1.1,-4.05]];
			rocket5a setVectorDirAndUp [[0.18,0.9,0.1],[0.82,0.1,0.9]];
			rocket6a attachTo [_aircraft,[-2.25,-1.1,-4.05]];
			rocket6a setVectorDirAndUp [[-0.18,0.9,0.1],[0.82,0.1,0.9]];
			rocket7a attachTo [_aircraft,[2.245,-1.1,-4.35]];	
			rocket7a setVectorDirAndUp [[0.2,0.9,0.1],[0.8,0.1,0.9]];	
			rocket8a attachTo [_aircraft,[-2.245,-1.1,-4.35]];
			rocket8a setVectorDirAndUp [[-0.2,0.9,0.1],[0.8,0.1,0.9]];
			
			// ... and smoke generators
			
			smoke1 = "Smoke_82mm_AMOS_White" createVehicle position _aircraft;
			smoke2 = "Smoke_82mm_AMOS_White" createVehicle position _aircraft;
			smoke3 = "Smoke_82mm_AMOS_White" createVehicle position _aircraft;
			smoke1 attachTo [_aircraft,[0,-1,-4]];
			smoke2 attachTo [_aircraft,[1.8,-10.5,-1.4]];
			smoke3 attachTo [_aircraft,[-1.8,-10.5,-1.4]];
		};
		
		// since the burn time of the M_AT9_AT is about 3-4 seconds, we need to replace the rockets from time to time
		
		if (_boostcycle == 10) then {
			rocket1b = _rockettype createVehicle position _aircraft;
			rocket1b enableSimulation false;
			rocket2b = _rockettype createVehicle position _aircraft;
			rocket2b enableSimulation false;
			rocket3b = _rockettype createVehicle position _aircraft;
			rocket3b enableSimulation false;
			rocket4b = _rockettype createVehicle position _aircraft;
			rocket4b enableSimulation false;
			rocket5b = _rockettype createVehicle position _aircraft;
			rocket5b enableSimulation false;
			rocket6b = _rockettype createVehicle position _aircraft;
			rocket6b enableSimulation false;
			rocket7b = _rockettype createVehicle position _aircraft;
			rocket7b enableSimulation false;
			rocket8b = _rockettype createVehicle position _aircraft;
			rocket8b enableSimulation false;
			rocket1b attachTo [_aircraft,[2.26,-1.1,-3.45]];
			rocket1b setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2b attachTo [_aircraft,[-2.26,-1.1,-3.45]];
			rocket2b setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
			rocket3b attachTo [_aircraft,[2.255,-1.1,-3.75]];
			rocket3b setVectorDirAndUp [[0.16,0.9,0.1],[0.84,0.1,0.9]];
			rocket4b attachTo [_aircraft,[-2.255,-1.1,-3.75]];
			rocket4b setVectorDirAndUp [[-0.16,0.9,0.1],[0.84,0.1,0.9]];
			rocket5b attachTo [_aircraft,[2.25,-1.1,-4.05]];
			rocket5b setVectorDirAndUp [[0.18,0.9,0.1],[0.82,0.1,0.9]];
			rocket6b attachTo [_aircraft,[-2.25,-1.1,-4.05]];
			rocket6b setVectorDirAndUp [[-0.18,0.9,0.1],[0.82,0.1,0.9]];
			rocket7b attachTo [_aircraft,[2.245,-1.1,-4.35]];	
			rocket7b setVectorDirAndUp [[0.2,0.9,0.1],[0.8,0.1,0.9]];	
			rocket8b attachTo [_aircraft,[-2.245,-1.1,-4.35]];
			rocket8b setVectorDirAndUp [[-0.2,0.9,0.1],[0.8,0.1,0.9]];
			deleteVehicle rocket1a;
			deleteVehicle rocket2a;
			deleteVehicle rocket3a;
			deleteVehicle rocket4a;
			deleteVehicle rocket5a;
			deleteVehicle rocket6a;
			deleteVehicle rocket7a;
			deleteVehicle rocket8a;
		};
		
		if (_boostcycle == 20) then {
			rocket1c = _rockettype createVehicle position _aircraft;
			rocket1c enableSimulation false;
			rocket2c = _rockettype createVehicle position _aircraft;
			rocket2c enableSimulation false;
			rocket3c = _rockettype createVehicle position _aircraft;
			rocket3c enableSimulation false;
			rocket4c = _rockettype createVehicle position _aircraft;
			rocket4c enableSimulation false;
			rocket5c = _rockettype createVehicle position _aircraft;
			rocket5c enableSimulation false;
			rocket6c = _rockettype createVehicle position _aircraft;
			rocket6c enableSimulation false;
			rocket7c = _rockettype createVehicle position _aircraft;
			rocket7c enableSimulation false;
			rocket8c = _rockettype createVehicle position _aircraft;
			rocket8c enableSimulation false;
			rocket1c attachTo [_aircraft,[2.26,-1.1,-3.45]];
			rocket1c setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2c attachTo [_aircraft,[-2.26,-1.1,-3.45]];
			rocket2c setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
			rocket3c attachTo [_aircraft,[2.255,-1.1,-3.75]];
			rocket3c setVectorDirAndUp [[0.16,0.9,0.1],[0.84,0.1,0.9]];
			rocket4c attachTo [_aircraft,[-2.255,-1.1,-3.75]];
			rocket4c setVectorDirAndUp [[-0.16,0.9,0.1],[0.84,0.1,0.9]];
			rocket5c attachTo [_aircraft,[2.25,-1.1,-4.05]];
			rocket5c setVectorDirAndUp [[0.18,0.9,0.1],[0.82,0.1,0.9]];
			rocket6c attachTo [_aircraft,[-2.25,-1.1,-4.05]];
			rocket6c setVectorDirAndUp [[-0.18,0.9,0.1],[0.82,0.1,0.9]];
			rocket7c attachTo [_aircraft,[2.245,-1.1,-4.35]];	
			rocket7c setVectorDirAndUp [[0.2,0.9,0.1],[0.8,0.1,0.9]];	
			rocket8c attachTo [_aircraft,[-2.245,-1.1,-4.35]];
			rocket8c setVectorDirAndUp [[-0.2,0.9,0.1],[0.8,0.1,0.9]];
			deleteVehicle rocket1b;
			deleteVehicle rocket2b;
			deleteVehicle rocket3b;
			deleteVehicle rocket4b;
			deleteVehicle rocket5b;
			deleteVehicle rocket6b;
			deleteVehicle rocket7b;
			deleteVehicle rocket8b;
		};
	};
	
	sleep 4.0;
	hint "JATO System dropped";
	
	// clean up the last set of rockets before they explode
	
	deleteVehicle rocket1c;
	deleteVehicle rocket2c;
	deleteVehicle rocket3c;
	deleteVehicle rocket4c;
	deleteVehicle rocket5c;
	deleteVehicle rocket6c;
	deleteVehicle rocket7c;
	deleteVehicle rocket8c;
	
	sleep 5.0;
	
	deleteVehicle smoke1;
	deleteVehicle smoke2;
	deleteVehicle smoke3;
	_aircraft allowdamage true;
};