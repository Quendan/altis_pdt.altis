//Loop which adds objects for the zeus to edit
//Done by eRazeri & Magoo
//execute from init.sqf: execVM "scripts\zeus\z_addall.sqf";
if (isServer) then 
{
sleep 6;
fnc_admin_1_zeus = {
      {
      Admin_1_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_1, ["AllVehicles","Man","thingX"], 200];//removed static so you cannot delete ins respawn points         
      };
fnc_admin_2_zeus = {
      {
      Admin_2_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_2, ["AllVehicles","Man","thingX"], 200];
      };      
fnc_admin_3_zeus = {
      {
      Admin_3_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_3, ["AllVehicles","Man","thingX"], 200]; 
      }; 
fnc_admin_4_zeus = {
      {
      Admin_4_z addCuratorEditableObjects [[_x],true];
      } forEach nearestObjects [position Admin_4, ["AllVehicles","Man","thingX"], 200];
      }; 
      
while {true} do 
   {
   if (!isNil "Admin_1") then {
   call fnc_admin_1_zeus
   };
   if (!isNil "Admin_2") then {
   call fnc_admin_2_zeus
   };
   if (!isNil "Admin_3") then {
   call fnc_admin_3_zeus
   };
   if (!isNil "Admin_4") then {
   call fnc_admin_4_zeus
   };
   sleep 5;      
   };
};