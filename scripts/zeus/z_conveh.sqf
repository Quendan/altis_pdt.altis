/*---------------------------------
Curator Construction Vehicle Script by [RG]Quendan
13 April 2014
16 April: Bugfixes (effective commander to commander)

This script enables the player who commands a particular vehicle to use the Zeus module to place down barriers.

Initialise in init.sqf ([construction_vehicle,construction_zeus] execVM "scripts\zeus\z_conveh.sqf". Each vehicle needs to have its own Zeus.
---------------------------------*/

if (isServer) then {

//Variable names
_veh =  _this select 0;
_zeus = _this select 1;
_radius = 20; // Radius of the editing area around the vehicle
_areaID = 1000; // Editing area ID. Set to 1000 to prevent overlapping with other areas (e.g. in RG-PDT)
_position = [0,0,0];

//Defining what Zeus may place. In this case Zeus can build fortification and shelters.
_zeus addCuratorAddons ["A3_Structures_F_Mil_Fortification","A3_Structures_F_Mil_Shelters"];
_zeus setCuratorEditingAreaType true; // Curator may only edit inside of editing area.
_zeus addCuratorEditingArea [_areaID,position (_this select 0),_radius];
_zeus addCuratorCameraArea [_areaID,position (_this select 0),_radius+20];
_zeus setCuratorCoef ["place",-0.02]; //Make items cheaper


while {alive _veh} do {
	
	if (((_position select 0) != ((position _veh) select 0)) || ((_position select 1) != ((position _veh) select 1))) then {
	_zeus removeCuratorEditingArea _areaID;
	_zeus removeCuratorCameraArea _areaID; 
	_zeus addCuratorEditingArea [_areaID,position (_this select 0),_radius];
	_zeus addCuratorCameraArea [_areaID,position (_this select 0),_radius+20];
	};
	_position = position _veh;
	
	_curator = commander _veh;
	_curator assignCurator _zeus;
	if ((commander _veh != _curator) || (isNull (commander _veh))) then {unassignCurator _zeus}; //Make sure people don't keep their powers
	
	sleep 5;
	
}; 

};