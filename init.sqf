//Disable that annoying saving stuff I hate so much
enableSaving[false,false];

// JIP Check (This code should be placed first line of init.sqf file)
if (!isServer && isNull player) then {isJIP=true;} else {isJIP=false;};

// Wait until player is initialized
if (!isDedicated) then {waitUntil {!isNull player && isPlayer player};};

[ //This is the settings array for qbt optimizer
	[
		// General settings
		10,	    // Time between loops (default 5s)
		false,	// Enable log file
		30,	    // Write to log interval (default 30s)		
		false	  // Enable debug
	],

	[
		// Server settings, -1 to disable		
		120,	// WeaponHolder stay time		
		50,	  // Immobile/destroyed vehicle stay time		
		120,	// Dead body stay time		
		900,	// Explosive stay time		
		3600,	// Mine stay time		
		600,	// Chemlight (& SmokeShell) stay time (class SmokeShell includes chemlights as well)		
		-1	// Disable unneeded AI features distance from nearest player
	],

	[
		// Client settings, -1 to disable		
		-1,	// Player & AI simulation distance	
		-1	// Distance where to find static objects to disable simulation of (qbt recommends 500), increase to save bandwidth, beware of issues it might cause
	]
] execVM "scripts\qbt_optimizer.sqf";

//Admin commands 
execVM "scripts\AdminLoop\init.sqf"; 

//Repair script
execVM "scripts\gvs\gvs_init.sqf";

//Suppression Script
_null = [2] execVM "scripts\tpwcas\tpwcas_script_init.sqf";

//setSkillz & nvg removal & GPS adder, looper designed for ALiVE & Zeus by eRazeri
[
1,//AI Skill settings, check the script for the exact skill settings
0,//Opfor nvg removal
0,//Blufor nvg removal
0,//RG nvg removal
0,//Add GPS to player if he hasnt got one
0,//Enable rpt logging
0 //Enable Extensive Skill rpt logging for debugging etc
] execVM "scripts\difficulty.sqf";

//Lock players in first person when moving/shooting etc
execVM "scripts\Launchthirdperson.sqf";

//Execute and load briefing
execVM "briefing.sqf";

//Vehicle Active Defence System
call compile preprocessFile "scripts\Baked_AIS\Baked_AIS_init.sqf";

// BTC Logistics
_logistic = execVM "scripts\=BTC=_Logistic\=BTC=_logistic_Init.sqf";

// INS_revive initialize
[] execVM "scripts\INS_revive\revive_init.sqf";

//Group manager, dependant on menu extension caller script and addon
execVM "scripts\group_manager\group_manager.sqf";

// Menu extension caller, this is also required currently by: Group Manager
execVM "scripts\MenuExtensionCall.sqf";

//DOM field repair script which allows engineers to repair vehicles
execVM "scripts\DOM_repair\init.sqf";

// Wait for INS_revive initialized
waitUntil {!isNil "INS_REV_FNCT_init_completed"};

//Mortar & HMG script for Gambler detachments
execVM "scripts\mortar_and_hmg_spawn\mortar_init.sqf";
execVM "scripts\mortar_and_hmg_spawn\hmg_init.sqf";

//Automatic ejection for F/A-18/Wipeout Pilot in case the jet is damaged beyond repair
execVM "scripts\f18\pilot_emergency_ejection.sqf";

//ACRE customization
[
1,//Enable terrain loss, default 1
0.5,//Terrain loss value, suggested value 0.5, default is 1
1,//Disable ingame automatic messages, suggested value 1
1//Enable retransmission network, depends on if you have antennas on the mission and want to use them.
]execVM "scripts\acre\acre_init.sqf";

//Recoil scripts (awesome!)
execVM "scripts\recoil\tank_recoil.sqf";
execVM "scripts\recoil\a10_recoil.sqf";

//JATO (Jet assisted takeoff/landing) Script for C-130J
execVM "scripts\JATO\jato.sqf";

//Start of PDT related scripts

//	Vehicle anti spawn-spam protection.
_handle = [] execVM "pdt_scripts\vehicleSpawn\spawnDelay.sqf";

//init range targets not to pop up after a while but only when needed to by radio commands.
nopop = true;

//remove civvies from zeus
if (isServer) then  {
sleep 1;
{_x removeCuratorEditableObjects [[civ1,civ2,civ3],false]} foreach allCurators;
};

//workaround for CTRG camos for requisition guys

if (isDedicated) then {
removeUniform veh_1_CTRG;
removeUniform veh_2_CTRG;
removeUniform veh_3_CTRG;
removeUniform helo_1_CTRG;
removeUniform plane_1_CTRG;
veh_1_CTRG addUniform "U_B_CTRG_1";
veh_2_CTRG addUniform "U_B_CTRG_1";
veh_3_CTRG addUniform "U_B_CTRG_1";
helo_1_CTRG addUniform "U_B_CTRG_1";
plane_1_CTRG addUniform "U_B_CTRG_1";
};

// Info text, displayed in bottom right corner
sleep 5;
	["15:00 EEST","July 6th 2035", "Stratis Island"] call BIS_fnc_infoText;
sleep 5;
	["Taskforce", "Reality", "Gaming"] call BIS_fnc_infoText;
sleep 5;
	["Personal", "Development", "Training"] call BIS_fnc_infoText;
  
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["static"], 3600];
sleep 1;
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["house"], 3600];
sleep 1;
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["Items_base_F"], 3600];
sleep 1;
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["Thing"], 3600];
sleep 1;
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["thingX"], 3600];
sleep 1;
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["Stall_base_F"], 3600];
sleep 1;
{_x enableSimulation false;_x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["Furniture_base_F"], 3600];
sleep 1;
{_x enableSimulation true; _x allowDamage false;} forEach nearestObjects [getMarkerPos "sim_dis", ["ReammoBox_F"], 3600];

{_x execVM "pdt_scripts\PopUpTarget.sqf"} forEach nearestObjects [getMarkerPos "sim_dis", ["TargetP_Inf_F"], 3600];